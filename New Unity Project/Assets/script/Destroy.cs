﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour {
	public GameObject explosion1;
	public GameControlScript gc1;
	public AudioClip AC1;
	// Use this for initialization
	void OnCollisionEnter(Collision coll)
	{
		print ("OTher thing:" + coll.gameObject.name);
		if (coll.gameObject.name == "Player") {
			Instantiate (explosion1, transform.position, transform.rotation);
			Destroy (gameObject);
			coll.gameObject.GetComponent<AudioSource> ().clip = AC1;
			coll.gameObject.GetComponent<AudioSource> ().Play ();
			gc1.map ();
		}
	}
}
