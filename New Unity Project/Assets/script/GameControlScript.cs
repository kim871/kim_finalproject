﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControlScript : MonoBehaviour {
	public GameObject timeIsUp, restartButton;
	public GameObject maincam;
	public GameObject mapcam;
	float maptimer;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Timer.timeLeft <= 0) {
			Time.timeScale = 0;
			timeIsUp.gameObject.SetActive (true);
			restartButton.gameObject.SetActive (true);
		}
		if (maptimer >= 0f) {
			maptimer -= Time.deltaTime;
			if (maptimer <= 0f) {
				toggleminimap (false);
			}
		}
	}
	public void restartScene()
	{
		timeIsUp.gameObject.SetActive (false);
		restartButton.gameObject.SetActive (false);
		Time.timeScale = 1;
		Timer.timeLeft = 60f;
		SceneManager.LoadScene ("main");
	}

	public void AddTime (int Seconds)
	{
		Timer.timeLeft += Seconds;
	}

	void UpdateTime ()
	{
	}

	public void toggleminimap (bool minimapon)
	{
		maincam.SetActive (!minimapon);
		mapcam.SetActive (minimapon);
	}
	public void map () {
		toggleminimap (true);
		maptimer = 9f;
	}

}
