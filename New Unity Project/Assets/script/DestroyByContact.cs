﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {
	public GameObject explosion;
	public GameControlScript gc;
	public AudioClip AC;
	void OnCollisionEnter(Collision col)
	{
		print ("OTher thing:" + col.gameObject.name);
		if (col.gameObject.name == "Player") {
			Instantiate (explosion, transform.position, transform.rotation);
			Destroy (gameObject);
			col.gameObject.GetComponent<AudioSource> ().clip = AC;
			col.gameObject.GetComponent<AudioSource> ().Play ();
			gc.AddTime (10);
		}
	}
}
