﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitSign : MonoBehaviour {
	public GameObject explosion2;
	public GameControlScript gc2;
	public AudioClip AC2;
	public GameObject exitmessage;
	public GameObject restart;
	// Use this for initialization
	void OnCollisionEnter(Collision colli)
	{
		print ("OTher thing:" + colli.gameObject.name);
		if (colli.gameObject.name == "Player") {
			Instantiate (explosion2, transform.position, transform.rotation);
			Destroy (gameObject);
			colli.gameObject.GetComponent<AudioSource> ().clip = AC2;
			colli.gameObject.GetComponent<AudioSource> ().Play ();
			exitmessage.SetActive (true);
			restart.SetActive (true);
			Time.timeScale = 0;
		}
	}
}
