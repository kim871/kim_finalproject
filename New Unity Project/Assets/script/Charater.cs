﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Charater : MonoBehaviour {
	public float moveSpeed;
	public float rotSpeed;
	Rigidbody rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		rb.MovePosition (transform.position + (transform.forward * moveSpeed * Input.GetAxis ("Vertical") * Time.deltaTime));
		Vector3 eulerAngles = rb.rotation.eulerAngles;
		rb.rotation = Quaternion.Euler (eulerAngles.x, eulerAngles.y + rotSpeed * Input.GetAxis ("Horizontal") * Time.deltaTime, eulerAngles.z);
		//transform.Translate (moveSpeed * Input.GetAxis ("Horizontal") * Time.deltaTime, 0f, moveSpeed * Input.GetAxis ("Vertical") * Time.deltaTime);
	}
}
